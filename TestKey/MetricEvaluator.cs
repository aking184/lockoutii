﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LockoutII
{

    public class KeyData
    {
        public int KeyValue { get; set; }       //Value to identify key
        public long KeyDown { get; set; }       //Key down time
        public long KeyUp { get; set; }         //Key up time

        public KeyData(int keyValue, long keyDown, long keyUp)
        {
            KeyValue = keyValue;
            KeyDown = keyDown;
            KeyUp = keyUp;
        }

        public double DwellTime()
        {
            double dwellTime = KeyUp - KeyDown;
            return dwellTime;
        }

    }

    public class Digraph
    {
        public int FirstKeyValue { get; set; }
        public long FirstKeyDown { get; set; }
        public long FirstKeyUp { get; set; }

        public int LastKeyValue { get; set; }
        public long LastKeyDown { get; set; }
        public long LastKeyUp { get; set; }

        public KeyData FirstKey { get; set; }
        public KeyData LastKey { get; set; }

        public Digraph(KeyData firstKey, KeyData lastKey)
        {
            FirstKey = firstKey;
            LastKey = lastKey;

            FirstKeyValue = firstKey.KeyValue;
            FirstKeyDown = firstKey.KeyDown;
            FirstKeyUp = firstKey.KeyUp;
            LastKeyValue = lastKey.KeyValue;
            LastKeyDown = lastKey.KeyDown;
            LastKeyUp = lastKey.KeyUp;
        }

        public double IntervalTime()
        {
            double intervalTime = LastKeyDown - FirstKeyUp;
            return intervalTime;
        }

        public double Latency()
        {
            double latency = LastKeyUp - FirstKeyDown;
            return latency;
        }

        public double FlightTime()
        {
            double flightTime = LastKeyDown - FirstKeyDown;
            return flightTime;
        }

        public double UpToUp()
        {
            double upToUp = LastKeyUp - FirstKeyUp;
            return upToUp;
        }

        public OrderedDictionary digraphValues()
        {
            OrderedDictionary digraphMetrics = new OrderedDictionary();
            digraphMetrics.Add("IT", IntervalTime());
            digraphMetrics.Add("DT", FirstKey.DwellTime());      //Dwell Time Is Attached to first key
            digraphMetrics.Add("LA", Latency());
            digraphMetrics.Add("FT", FlightTime());
            digraphMetrics.Add("UU", UpToUp());
            return digraphMetrics;
        }
    }

    public class ListBuilder : List<KeyData>
    {
        List<KeyData> KeyList = new List<KeyData>();
       // UserData myUserData = new UserData();
        UserData MyUserData { get; set; }
        public string Username { get; set; }

        public ListBuilder(string username)
        {
            Username = username;
            UserData myUserData = new UserData(Username);
            MyUserData = myUserData;
            List<KeyData> keyList = new List<KeyData>();
            KeyList = keyList;
            myUserData.Username = Username; 
        }

        //Function to change is Authenticating in current UserData
        public void UpdateIsAuthenticating( bool isChecked)
        {
            MyUserData.IsAuthenticating = isChecked;
        }

        //Function to change Adaptive Learning in current UserData
        public void UpdateAdaptiveLearning(bool isChecked)
        {
            MyUserData.AdaptiveLearning = isChecked;
        }

        //Function to change TrustThreshold in current UserData
        public void UpdateTrustThreshold(int newTrustThreshold)
        {
            MyUserData.TrustThreshold = newTrustThreshold;
        }

        //Function to change Lockout Distance in current UserData 
        public void UpdateThresholdDistance(int newThresholdDistance)
        {
            MyUserData.ThresholdDistance = newThresholdDistance;
        }

        //Function takes KeyDatas on KeyUp and modifies the Correct Key Element
        public int OnKeyUp(KeyData keyUpData)
        {
            int keyValue = keyUpData.KeyValue;
            long keyUpTime = keyUpData.KeyUp;

            int keyListIndex = 0;

            //Find node in ArrayList with matching KeyValue where keyUp time = -1
            for (int i = 0; i < this.Count(); i++)
            {
                KeyData key = this[i] as KeyData;

                if (key.KeyValue == keyValue && key.KeyUp == -1)
                {
                    key.KeyUp = keyUpTime;
                    keyListIndex = this.IndexOf(key);
                    break;
                }
            }
            return keyListIndex;
        }

        //Function that processes a key in KeyList based on its index
        public void KeyListProcessor(int keyListIndex)
        {
            KeyData currentKey = this[keyListIndex] as KeyData;
            bool digraphNext = KeyListNext(keyListIndex);
            bool digraphPrevious = KeyListPrevious(keyListIndex);

            if (digraphPrevious == true && digraphNext == true)
            {
                this.RemoveAt(keyListIndex);
            }

        }

        public bool KeyListNext(int keyListIndex)
        {
            KeyData currentKey = this[keyListIndex] as KeyData;
            //KeyData currentKey = (KeyData)KeyList[keyListIndex]; 
            int keyListCount = this.Count;
            bool digraphNext = false;       //Processed Digraph with the next key

            if (keyListIndex < keyListCount - 1)
            {
                KeyData nextKey = this[keyListIndex + 1] as KeyData;
                //KeyData nextKey = (KeyData) KeyList[keyListIndex + 1];

                if (nextKey.KeyUp != -1)
                {
                    DigraphProcess(currentKey, nextKey);
                    digraphNext = true;

                    //Make this a Recursive removeheck function
                    if (keyListIndex < keyListCount - 2)
                    {
                        KeyData nextNextKey = this[keyListIndex + 2] as KeyData;
                        //KeyData nextNextKey = (KeyData)KeyList[keyListIndex + 2];

                        if (nextNextKey.KeyUp != -1)
                        {
                            this.RemoveAt(keyListIndex + 1);
                        }
                    }
                }
            }

            return digraphNext;
        }

        public bool KeyListPrevious(int keyListIndex)
        {

            KeyData currentKey = this[keyListIndex] as KeyData;
            // KeyData currentKey = (KeyData)KeyList[keyListIndex]; 

            bool digraphPrevious = false;   //Processed Digraph with previous key

            if (keyListIndex > 0)
            {
                KeyData previousKey = this[keyListIndex - 1] as KeyData;
                //KeyData previousKey = (KeyData)KeyList[keyListIndex - 1];

                if (previousKey.KeyUp != -1)
                {
                    DigraphProcess(previousKey, currentKey);
                    digraphPrevious = true;

                    //Make this a recursive removeCheck function
                    if (keyListIndex > 1)
                    {
                        KeyData previousPreviousKey = this[keyListIndex - 2] as KeyData;
                        //KeyData previousPreviousKey = (KeyData)KeyList[keyListIndex - 2];

                        if (previousPreviousKey.KeyUp != -1)
                        {
                            this.RemoveAt(keyListIndex - 1);
                        }
                    }
                }
            }

            return digraphPrevious;
        }


        //Funtion that processes the digraphs
        public void DigraphProcess(KeyData firstKey, KeyData lastKey)
        {
            //Make a Digraph instance
            Digraph myDigraph = new Digraph(firstKey, lastKey);
            //Pass to new metric evaluator
            MetricEvaluator myMetricEvaluator = new MetricEvaluator(firstKey.KeyValue, lastKey.KeyValue, myDigraph.digraphValues(), MyUserData.Username);
            myMetricEvaluator.PhaseCheck(MyUserData);

            //Update UserData
            MyUserData = myMetricEvaluator.GetMetricUserData();
        }

        public String GetConfidenceLevel()
        {
            string confidenceLevel = Convert.ToString(MyUserData.ConfidenceLevel);
            return confidenceLevel;
        }
    }

    public class MetricEvaluator
    {
        //UserData myUserData = new UserData();
        private const Double outlierTime = 1000;
        UserData MyUserData { get; set; }
        string Username { get; set; }
        int KeyValueOne { get; set; }
        int KeyValueTwo { get; set; }
        OrderedDictionary DigraphMetrics { get; set; }

        public MetricEvaluator(int keyValueOne, int keyValueTwo, OrderedDictionary digraphMetrics, string username)
        {      
            Username = username;
            UserData myUserData = new UserData(Username);
            KeyValueOne = keyValueOne;
            KeyValueTwo = keyValueTwo;
            DigraphMetrics = digraphMetrics;
        }

        public void PhaseCheck(UserData listUserData)
        {
           //Update User Data
           MyUserData = listUserData;

           if (MyUserData.IsAuthenticating == true)
            {
                this.Authenticator();
            }
            else
            {
                this.EnrollmentWriter();
            }
        }

        //Return ConfidenceLevel in string form for updating Data
        public UserData GetMetricUserData()
        {
            return MyUserData;
        }


        public void Authenticator()
        {
            LockoutIIDB myLockoutIIDB = new LockoutIIDB();

            //Key Metric
            double distanceSum = 0;
            double distanceCount = 0;
            double varianceSum = 0;

           //Loop through DigraphMetrics
           foreach (DictionaryEntry keyMetric in DigraphMetrics)
           {
               String metricID = (String)keyMetric.Key;
               double metricValue = (double)keyMetric.Value;

               //Check for outliers
               if (metricValue < outlierTime)
               {
                   //Check if given dwell time is in template
                   if (metricID == "DT")
                   {
                       double[] templateMetrics = myLockoutIIDB.GetTemplateData(KeyValueOne, KeyValueOne, metricID);

                       //If Template Contains DwellTime, process distance and modify confidence accordingly
                       if (templateMetrics[0] != 0)
                       {
                           double distance = this.SingleMetricManhattan(templateMetrics[0], templateMetrics[1], metricValue);
                           //double distance = this.WeightedManhattan(templateMetrics[0], templateMetrics[1], metricValue);

                           //Modify Confidence Level Accordingly
                           this.DistanceMetricProcessor(distance, templateMetrics[1], 1);
                       }
                       else
                       {
                           //Metric was not found in template; Slightly decrease confidence
                           MyUserData.ConfidenceLevel -= 0.0001;

                           //Check if confidence level is below threshold
                           MyUserData.LockoutCheck();
                       }
                   }
                   else
                   {
                       double[] templateMetrics = myLockoutIIDB.GetTemplateData(KeyValueOne, KeyValueTwo, metricID);

                       //Metrics other than dwell time
                       if (templateMetrics[0] != 0)
                       {
                           double distance = this.SingleMetricManhattan(templateMetrics[0], templateMetrics[1], metricValue);
                           //double distance = this.WeightedManhattan(templateMetrics[0], templateMetrics[1], metricValue);

                           distanceSum = distanceSum + distance;
                           varianceSum = varianceSum + templateMetrics[1];
                           distanceCount++;
                       }
                       else
                       {
                           //Metric was not found in template; Slightly decrease confidence
                           MyUserData.ConfidenceLevel -= 0.0001;

                           //Check if confidence level is below threshold
                           MyUserData.LockoutCheck();
                       }
                   }
               }
           }

           this.DistanceMetricProcessor(distanceSum, varianceSum, distanceCount);

        }

        //Process Distances generated in Authenticator
        public void DistanceMetricProcessor(double distanceSum, double varianceSum, double distanceCount)
        {
            if (distanceCount > 0)
            {
                //Calculate the average distance for Manhattan
               double averageDistance = distanceSum / distanceCount;

                //Calculate the average distance for Weighted Manhattan
                //double averageDistance = distanceSum / varianceSum;
               
                //Calculate averageVariance; this will be the distance threshold
                //double averageVariance = varianceSum / distanceCount;

                //Calculate increase or decrease in confidence level
                //double confidenceUpdate = (averageVariance - averageDistance) / (averageVariance);

                //Set Max Punishment
                //confidenceUpdate = Math.Max(confidenceUpdate, -2.23);

                //Set Max Reward
                //confidenceUpdate = Math.Min(confidenceUpdate, 0.33);

                //Alternate Update
                double confidenceUpdate = this.ConUpdate(averageDistance);

                //Adaptive learning
                this.LearningCheck(confidenceUpdate);

                //Update confidence level
                MyUserData.ConfidenceLevel = Math.Min(MyUserData.ConfidenceLevel + confidenceUpdate, 100);

                //Check if confidence level is below threshold
                MyUserData.LockoutCheck();
            }
        }

        //Alternate Confidence Update Mechanism
        private double ConUpdate(double averageDistance)
        {
            double thresholdDistance = MyUserData.ThresholdDistance;
            double reward = 0.25;
            double confidenceUpdate = 0;
            double maxPunishment = -1;

            if (averageDistance < thresholdDistance)
            {
                confidenceUpdate = reward;
            }
            else if (averageDistance >= thresholdDistance)
            {
                confidenceUpdate = Math.Max(-1*(averageDistance - thresholdDistance), maxPunishment);
            }

            return confidenceUpdate;
        }



        public void LearningCheck(double confidenceUpdate)
        {
            //Perform adaptive learning check when confidence update is positive
            if (confidenceUpdate > 0 && MyUserData.AdaptiveLearning == true)
            {
                //Update enrollment
                this.EnrollmentWriter();
            }
        }

        //Find Manhattan Distance for a Single Metric (e.g. DwellTime)
        public double SingleMetricManhattan(double templateMean, double templateVariance, double keyMetric)
        {
            double distance = (Math.Abs(templateMean - keyMetric)) / templateVariance;
            return distance;
        }

        /*The methods below are usesed for alternative distance algorithms */
        //Find "Weighted" Manhattan Distance for a Single Metric (e.g. DwellTime)
        public double WeightedManhattan(double templateMean, double templateVariance, double keyMetric)
        {
            double distance = (Math.Abs(templateMean - keyMetric));
            return distance;
        }

       /*
        //Find Weighted Euclidean Distance for a Single Metric (e.g. DwellTime)
        //The Weighted Euclidean does not really work
        public double WeightedEuclidean(double templateMean, double templateVariance, double keyMetric)
        {
            double distance = templateVariance * Math.Pow((Math.Abs(templateMean - keyMetric)),2);
            return distance;
        }*/
        /*The methods above are usesed for alternative distance algorithms*/


        public double[] getStabilityMetrics(String metricID)
        {
            //QueryDB For Mean, Variance, and N
            LockoutIIDB myLockoutIIDB = new LockoutIIDB();
            if (metricID == "DT")
            {
                double[] stabilityMetrics = myLockoutIIDB.GetMetricData(KeyValueOne, KeyValueOne, metricID, Username);
                return stabilityMetrics;
            }
            else
            {
                double[] stabilityMetrics = myLockoutIIDB.GetMetricData(KeyValueOne, KeyValueTwo, metricID, Username);
                return stabilityMetrics;
            }
            
        }

        private bool KeyCheck()
        {
            //BackSpace (8) is not a good key to authenticate by
            int[] unallowedKeys = new int[] { 8 };

            bool keysAllowed = true;
            foreach (int keyValue in unallowedKeys)
            {
                if (KeyValueOne == keyValue || KeyValueTwo == keyValue)
                {
                    keysAllowed = false;
                    break;
                }

            }
            return keysAllowed;
        }



        //Write Non-outlier values to Enrollemnt
        public void EnrollmentWriter()
        {
            LockoutIIDB myLockoutIIDB = new LockoutIIDB();

            foreach (DictionaryEntry keyMetric in DigraphMetrics)
            {
                String metricID = (String)keyMetric.Key;
                double metricValue = (double)keyMetric.Value;

                //Check for keyValues that don't make good metric
                //bool keysAllowed = this.KeyCheck();
                bool keysAllowed = true;

                //Pull out outliers and backspace key
                if (metricValue < outlierTime && keysAllowed == true)
                {
                    //Query DB For stability Metrics
                    double[] stabilityMetrics = getStabilityMetrics(metricID);

                    //Label Stability Metrics
                    double oldMean = stabilityMetrics[0];
                    double oldVariance = stabilityMetrics[1];
                    int oldN = (int)stabilityMetrics[2];

                    //Calculate New Mean
                    double newMean = NewMean(oldMean, oldN, metricValue);

                    //Calculate Standard Deviation
                    double newSDev = NewSDev(oldVariance, oldMean, newMean, metricValue, oldN);

                    //Calculate newN
                    int newN = oldN + 1;

                    //Write new values to DB
                    if (metricID == "DT")
                    {
                        myLockoutIIDB.updateKeyMetric(KeyValueOne, KeyValueOne, metricID, newMean, newSDev, newN, Username);
                    }
                    else
                    { 
                        myLockoutIIDB.updateKeyMetric(KeyValueOne, KeyValueTwo, metricID, newMean, newSDev, newN, Username);
                    }
                }
            }
        }

        public double NewMean(double oldMean, int oldN, double metricValue)
        {
            double newMean = ((oldN * oldMean) + metricValue) / (oldN + 1);
            return newMean;
        }

        public double NewSDev(double oldSDev, double oldMean, double newMean, double metricValue, int oldN)
        {
            if (oldN == 0)
            {
                return 0;
            }

           // double sumXiSquared = (oldVariance * (oldN - 1)) + (oldMean * oldMean / oldN);
           // double sXX = (sumXiSquared + (metricValue * metricValue)) - (newMean * newMean / (oldN + 1));
           // double newVariance = sXX / oldN;
           //return newVariance;
            
            double oldVariance = oldSDev * oldSDev;
            double sumXiSquared = (oldVariance * (oldN - 1)) + (oldMean * oldMean * oldN);
            double sXX = (sumXiSquared + (metricValue * metricValue)) - (newMean * newMean * (oldN + 1));
            double newVariance = sXX / oldN;
            return Math.Sqrt(newVariance);

        }

    }

    public class UserData
    {

        //Note that these set methods will not change in database
        public string Username { get; set; }
        public int ThresholdDistance { get; set; }
        public double ConfidenceLevel { get; set; }
        public int TrustThreshold { get; set; }
        public bool AdaptiveLearning { get; set; }
        public bool IsAuthenticating { get; set; }

        public UserData(string username)
        {
            Username = username;
            LockoutIIDB myLockoutIIDB = new LockoutIIDB();
            ArrayList userArrayList = myLockoutIIDB.GetUserData(Username);

            //Fix these casts
            TrustThreshold = Convert.ToInt32((double)userArrayList[0]);
            ConfidenceLevel = 100;
            AdaptiveLearning = (bool) userArrayList[1];
            ThresholdDistance = (short) userArrayList[2];
            IsAuthenticating = (bool) userArrayList[3];

        }


        public void LockoutCheck()
        {
            if (this.ConfidenceLevel < this.TrustThreshold)
            {
                //MessageBox.Show("Lockout!!");

                //Reset Confidence Level
                this.ConfidenceLevel = 100;

               //Record Lockout In Table
                LockoutIIDB myLockoutIIDB = new LockoutIIDB();

                //LK --> Lockout
                myLockoutIIDB.UpdateLogTable("LK", Username);   
                
                //Lockout User
                System.Diagnostics.Process.Start("Rundll32.exe", "User32.dll,LockWorkStation");
            }

        }

    }
}
