﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using Gma.UserActivityMonitor;

namespace LockoutII
{
    public partial class Form1 : Form
    {
        System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();
        //ListBuilder keyList = new ListBuilder();
        LockoutIIDB myLockoutIIDB = new LockoutIIDB();
        public ListBuilder KeyList { get; set; }
        public string ThisUser { get; set; }   //UserName

        public Form1(string thisUser)
        {
            //Specify User
            ThisUser = thisUser;
            ListBuilder keyList = new ListBuilder(ThisUser);
            KeyList = keyList;

            InitializeComponent();
            myStopwatch.Start();
            //Query DB to make template
            this.BuildUserTemplate();
            this.initUserData();
            this.ShowEnrollmentForm();
            this.loginUpdate();
            //this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(textBox1_KeyDown);
            //this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(textBox1_KeyUp);          
            HookManager.KeyDown += HookManager_KeyDown;
            HookManager.KeyUp += HookManager_KeyUp;
            this.FormClosing += new FormClosingEventHandler(this.Form1_FormClosing);
        }

        private void loginUpdate()
        {
            //LI --> LogIn
            myLockoutIIDB.UpdateLogTable("LI" , ThisUser);

            //Clean Log Table
            myLockoutIIDB.CleanLogTable(ThisUser);
        }

        private void ShowEnrollmentForm()
        {
            //If we are not authenticating show form by default
            if (isAuthenticatingCheckBox.Checked == false)
            {
                //Show Form
                this.SetVisibleCore(true);
            } 
       }

        private void initUserData()
        {
            //Set Trust Threshold adaptive learning, lockoutTime and isAuthenticating with initial user data
           ArrayList userdataArray =  myLockoutIIDB.GetUserData(ThisUser);

           //Trust Threshold Label UpdateLabel
           trustThreshold.Text =Convert.ToString(userdataArray[0]);
           thresholdTrackBar.Value = Convert.ToInt32((double)userdataArray[0]);
           adaptiveLearning.Checked= (bool)userdataArray[1];
           thresholdDistance.Value = (short)userdataArray[2];
           isAuthenticatingCheckBox.Checked = (bool)userdataArray[3];
           isEnrollingCheckBox.Checked = (bool)userdataArray[4];

        }

        private void HookManager_KeyDown(object sender, KeyEventArgs e)
        {
            //Only perform operations if we are enrolling or authenticating
            if (isEnrollingCheckBox.Checked || isAuthenticatingCheckBox.Checked)
            {
                this.ShowHideForm(sender, e);
                long keyDownTime = myStopwatch.ElapsedMilliseconds;
                int keyValue = e.KeyValue;
                KeyData keyDown = new KeyData(keyValue, keyDownTime, -1);  //-1 is a placeholder
                KeyList.Add(keyDown);
                //textBox1.AppendText(string.Format("{0}\n", e.KeyCode));
            }
        }

        private void HookManager_KeyUp(object sender, KeyEventArgs e)
        {
            //Only perform operations if we are enrolling or authenticating
            if (isEnrollingCheckBox.Checked || isAuthenticatingCheckBox.Checked)
            {
                long keyUpTime = myStopwatch.ElapsedMilliseconds;
                stopWatchReset(keyUpTime);
                int keyValue = e.KeyValue;
                KeyData keyUp = new KeyData(keyValue, -1, keyUpTime);  //-1 is a placeholder
                int keyListIndex = KeyList.OnKeyUp(keyUp);
                KeyList.KeyListProcessor(keyListIndex);

                //UpdateConfidence Level
                label2.Text = KeyList.GetConfidenceLevel();
            }
        }

        private void stopWatchReset(long keyTime)
        {
            long maxTime = 1000000000000;   //ms
            if (keyTime > maxTime)
            {
                myStopwatch.Stop();
                KeyList.Clear();
                myStopwatch.Start();
            }
        }

       private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(textBox1_KeyDown);
        }

       private void ShowHideForm(object sender, KeyEventArgs e)
       {
           if (e.KeyCode == Keys.F11) //F11
           {
               //Show Form
               //this.SetVisibleCore(true);
               this.AuthenticateUser();
           }
           else if (e.KeyCode == Keys.F8) //F8
           {
               //Hide Form
               this.SetVisibleCore(false);
           }
       }

       private void AuthenticateUser()
       {
           if (Visible == false)
           {
               AuthenticationForm myAuthenticationForm = new AuthenticationForm(ThisUser);
               myAuthenticationForm.Show();
           }

       }

       private void BuildUserTemplate()
       {
           if (isAuthenticatingCheckBox.Checked)
           {
               myLockoutIIDB.buildTemplate(ThisUser);
           }
       }


       private void isAuthenticatingCheckBox_CheckedChanged(object sender, EventArgs e)
       {
           string boolName = "IsAuthenticating";
           KeyList.UpdateIsAuthenticating(isAuthenticatingCheckBox.Checked);
           myLockoutIIDB.UpdateUserBool(boolName, isAuthenticatingCheckBox.Checked, ThisUser);
           this.BuildUserTemplate();

          //We Cannot be "enrolling" and authenticating at the same time
           if (isAuthenticatingCheckBox.Checked && isEnrollingCheckBox.Checked)
           {
               //Remove Is Enrolling
               boolName = "IsEnrolling";
               isEnrollingCheckBox.Checked = false;
               myLockoutIIDB.UpdateUserBool(boolName, false, ThisUser);
           }

           //If we are not authenticating then we cannot implement "adaptive learning"
           if (!isAuthenticatingCheckBox.Checked && adaptiveLearning.Checked)
          {
              boolName = "adaptive_template";
              adaptiveLearning.Checked = false;
              KeyList.UpdateAdaptiveLearning(false);
              myLockoutIIDB.UpdateUserBool(boolName, false, ThisUser);
          }
       }

       private void adaptiveLearning_CheckedChanged(object sender, EventArgs e)
       {
           KeyList.UpdateAdaptiveLearning(adaptiveLearning.Checked);
           myLockoutIIDB.UpdateUserBool("adaptive_template", adaptiveLearning.Checked, ThisUser);

           //If adaptive learning is checked, enrolling is not checked
           if (adaptiveLearning.Checked && isEnrollingCheckBox.Checked)
           {
               //Remove IsEnrolling
               isEnrollingCheckBox.Checked = false;
               myLockoutIIDB.UpdateUserBool("IsEnrolling", false, ThisUser);
           }

           //If adaptive learning is checked, isAuthenticating must also be checked
           if (adaptiveLearning.Checked && !isAuthenticatingCheckBox.Checked)
           {
               //Set isAuthenticating to true
               isAuthenticatingCheckBox.Checked = true;
               KeyList.UpdateIsAuthenticating(true);
               myLockoutIIDB.UpdateUserBool("IsAuthenticating", true, ThisUser);
               this.BuildUserTemplate();
           }

       }

       private void thresholdTrackBar_Scroll(object sender, EventArgs e)
       {
           //Upadate DB
           myLockoutIIDB.UpdateThreshold(thresholdTrackBar.Value, ThisUser);
           KeyList.UpdateTrustThreshold(thresholdTrackBar.Value);
           //UpdateLabel
           trustThreshold.Text = Convert.ToString(thresholdTrackBar.Value);
       }

      /*private void lockoutTimeSetter_ValueChanged(object sender, EventArgs e)
       {
           int lockoutTime = (int)lockoutTimeSetter.Value;
           KeyList.UpdateLockoutTime(lockoutTime);
           myLockoutIIDB.UpdateLockoutTime(lockoutTime, ThisUser);
       }*/

       private void Form1_Load(object sender, EventArgs e)
       {

       }

       //Quit Application when form closes.
       private void Form1_FormClosing(object sender, FormClosingEventArgs e)
       {
           //EX --> Exit
           myLockoutIIDB.UpdateLogTable("EX", ThisUser); 
           Application.Exit();
       }

       private void btnLogData_Click(object sender, EventArgs e)
       {
           //View Logs
           //Make Text Box Show User Logs
           string logString = myLockoutIIDB.GetLogData(ThisUser);
           textBox1.Text = logString;
       }

       private void isEnrollingCheckBox_CheckedChanged(object sender, EventArgs e)
       {
           myLockoutIIDB.UpdateUserBool("IsEnrolling", isEnrollingCheckBox.Checked, ThisUser);

          //Set Adaptive Learning to false
           if (isEnrollingCheckBox.Checked && adaptiveLearning.Checked)
           {
               adaptiveLearning.Checked = false;
               KeyList.UpdateAdaptiveLearning(false);
               myLockoutIIDB.UpdateUserBool("adaptive_template", false, ThisUser);
           }
           
           //Set Authenticating to false
           if (isEnrollingCheckBox.Checked && isAuthenticatingCheckBox.Checked)
           {
               isAuthenticatingCheckBox.Checked = false;
               KeyList.UpdateIsAuthenticating(false);
               myLockoutIIDB.UpdateUserBool("IsAuthenticating", false, ThisUser);
           }
       }

       private void userInfoBtn_Click(object sender, EventArgs e)
       {
           //Disable form an hide it
           this.Hide();
           this.Enabled = false;
           this.LoadUserInfoForm();
       }

       private void LoadUserInfoForm()
       {
           UserInfoForm myUserInfoForm = new UserInfoForm(ThisUser);
           myUserInfoForm.Show();
       }

       private void thresholdDistance_ValueChanged(object sender, EventArgs e)
       {
           int thresholdDistanceInt = (int)thresholdDistance.Value;
           KeyList.UpdateThresholdDistance(thresholdDistanceInt);
           myLockoutIIDB.UpdateThresholdDistance(thresholdDistanceInt, ThisUser);
       }


    }
}
