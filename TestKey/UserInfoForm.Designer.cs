﻿namespace LockoutII
{
    partial class UserInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserInfoForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.oldUserTextBox = new System.Windows.Forms.TextBox();
            this.newUserTextBox = new System.Windows.Forms.TextBox();
            this.submitBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.oldPasswordTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.newPassTextBox = new System.Windows.Forms.TextBox();
            this.retypeNewPassTextBox = new System.Windows.Forms.TextBox();
            this.cancelBTN = new System.Windows.Forms.Button();
            this.checkBoxPass = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "New Username:";
            // 
            // oldUserTextBox
            // 
            this.oldUserTextBox.Location = new System.Drawing.Point(132, 21);
            this.oldUserTextBox.Name = "oldUserTextBox";
            this.oldUserTextBox.ReadOnly = true;
            this.oldUserTextBox.Size = new System.Drawing.Size(125, 22);
            this.oldUserTextBox.TabIndex = 3;
            this.oldUserTextBox.TextChanged += new System.EventHandler(this.oldUserTextBox_TextChanged);
            // 
            // newUserTextBox
            // 
            this.newUserTextBox.Location = new System.Drawing.Point(132, 65);
            this.newUserTextBox.Name = "newUserTextBox";
            this.newUserTextBox.Size = new System.Drawing.Size(125, 22);
            this.newUserTextBox.TabIndex = 4;
            // 
            // submitBtn
            // 
            this.submitBtn.Location = new System.Drawing.Point(21, 281);
            this.submitBtn.Name = "submitBtn";
            this.submitBtn.Size = new System.Drawing.Size(75, 32);
            this.submitBtn.TabIndex = 5;
            this.submitBtn.Text = "Submit";
            this.submitBtn.UseVisualStyleBackColor = true;
            this.submitBtn.Click += new System.EventHandler(this.submitBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Password:";
            // 
            // oldPasswordTextBox
            // 
            this.oldPasswordTextBox.Location = new System.Drawing.Point(132, 112);
            this.oldPasswordTextBox.Name = "oldPasswordTextBox";
            this.oldPasswordTextBox.PasswordChar = '*';
            this.oldPasswordTextBox.Size = new System.Drawing.Size(125, 22);
            this.oldPasswordTextBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "New Password:";
            // 
            // newPassTextBox
            // 
            this.newPassTextBox.Location = new System.Drawing.Point(132, 156);
            this.newPassTextBox.Name = "newPassTextBox";
            this.newPassTextBox.PasswordChar = '*';
            this.newPassTextBox.Size = new System.Drawing.Size(125, 22);
            this.newPassTextBox.TabIndex = 9;
            // 
            // retypeNewPassTextBox
            // 
            this.retypeNewPassTextBox.Location = new System.Drawing.Point(132, 200);
            this.retypeNewPassTextBox.Name = "retypeNewPassTextBox";
            this.retypeNewPassTextBox.PasswordChar = '*';
            this.retypeNewPassTextBox.Size = new System.Drawing.Size(125, 22);
            this.retypeNewPassTextBox.TabIndex = 10;
            this.retypeNewPassTextBox.TextChanged += new System.EventHandler(this.retypeNewPassTextBox_TextChanged);
            // 
            // cancelBTN
            // 
            this.cancelBTN.Location = new System.Drawing.Point(182, 281);
            this.cancelBTN.Name = "cancelBTN";
            this.cancelBTN.Size = new System.Drawing.Size(75, 32);
            this.cancelBTN.TabIndex = 11;
            this.cancelBTN.Text = "Cancel";
            this.cancelBTN.UseVisualStyleBackColor = true;
            this.cancelBTN.Click += new System.EventHandler(this.cancelBTN_Click);
            // 
            // checkBoxPass
            // 
            this.checkBoxPass.AutoSize = true;
            this.checkBoxPass.Location = new System.Drawing.Point(132, 245);
            this.checkBoxPass.Name = "checkBoxPass";
            this.checkBoxPass.Size = new System.Drawing.Size(129, 21);
            this.checkBoxPass.TabIndex = 12;
            this.checkBoxPass.Text = "Show Password";
            this.checkBoxPass.UseVisualStyleBackColor = true;
            this.checkBoxPass.CheckedChanged += new System.EventHandler(this.checkBoxPass_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 34);
            this.label5.TabIndex = 13;
            this.label5.Text = "Confim \r\nPassword:";
            // 
            // deleteBtn
            // 
            this.deleteBtn.Location = new System.Drawing.Point(101, 281);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(75, 32);
            this.deleteBtn.TabIndex = 14;
            this.deleteBtn.Text = "Delete";
            this.deleteBtn.UseVisualStyleBackColor = true;
            this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // UserInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 333);
            this.Controls.Add(this.deleteBtn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.checkBoxPass);
            this.Controls.Add(this.cancelBTN);
            this.Controls.Add(this.retypeNewPassTextBox);
            this.Controls.Add(this.newPassTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.oldPasswordTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.submitBtn);
            this.Controls.Add(this.newUserTextBox);
            this.Controls.Add(this.oldUserTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserInfoForm";
            this.Text = "User Form";
            this.Load += new System.EventHandler(this.UserInfoForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox oldUserTextBox;
        private System.Windows.Forms.TextBox newUserTextBox;
        private System.Windows.Forms.Button submitBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox oldPasswordTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox newPassTextBox;
        private System.Windows.Forms.TextBox retypeNewPassTextBox;
        private System.Windows.Forms.Button cancelBTN;
        private System.Windows.Forms.CheckBox checkBoxPass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button deleteBtn;
    }
}