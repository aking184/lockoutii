﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LockoutII
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Generate Tables
            LockoutIIDB myLockoutIIDB = new LockoutIIDB();
            myLockoutIIDB.GenerateTables();

            Application.Run(new LoginForm());
        }
    }
}
