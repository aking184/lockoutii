﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LockoutII
{
    public partial class NewUserForm : Form
    {

        //Instantiate Database
        LockoutIIDB myLockoutIIDB = new LockoutIIDB();

        public NewUserForm()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void addUserBtn_Click(object sender, EventArgs e)
        {
            //strip any special charactars from username
            string username = this.StripChars(newUserTextBox.Text);

            //strip special characters from password
            string password = this.StripChars(newPassTextBox.Text);

            //Check if username is already in system
             bool userExists = this.CheckUsername(username);

            //Check if passwords match
            bool checkPasswords = this.checkPassword();
            
            if (userExists == false && checkPasswords == true)
            {
                //Add new user to database
                myLockoutIIDB.AddUser(username, password);

                //load main form
                this.LoadMainForm(username);

                //hide Current Forms
                this.HideCurrentForms();
            }
            else if (userExists == false && checkPasswords == false)
            {
                this.ClearPasswords();
                //prompt user to change passwords
                MessageBox.Show("Passwords do not match");
            }
            else
            {
                this.ClearForm();
                MessageBox.Show("Unable to add user");
            }

        }

        private bool CheckUsername( string username)
        {
            
            //Return bool
            bool userExists = false;

            //Check if Database has username
            int hasUser = myLockoutIIDB.HasUser(username);

            //If the database has the username show message box
            if (hasUser >= 1)
            {
                userExists = true;
            }

            return userExists;
        }


        private bool checkPassword()
        {
            bool passwordMatch = false;
            if (newPassTextBox.Text == retypePasswordTextBox.Text)
            {
                passwordMatch = true;
            }
            return passwordMatch;
        }

        private void ClearForm()
        {
            newUserTextBox.Text = " ";
            this.ClearPasswords();
        }

        private void ClearPasswords()
        {
            newPassTextBox.Text = "";
            retypePasswordTextBox.Text = "";
        }


        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Owner.Show();
            this.Dispose();
        }

        private void checkPass_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPass.Checked == true)
            {
                newPassTextBox.PasswordChar = (char)0; //reset passwordchar to default
                retypePasswordTextBox.PasswordChar = (char)0; //reset passwordchar to default
                checkPass.Text = "Hide password";
            }
            else
            {
                newPassTextBox.PasswordChar = '*'; //set passwordchar to '*'
                retypePasswordTextBox.PasswordChar = '*'; //set passwordchar to '*'
                checkPass.Text = "Show password";
            }
        }

        private string StripChars(string userInput)
        {
            //Replace apostophres to prevent SQL injections
            userInput.Replace("'", "''");

            //Check for SQL Injection attack
            userInput.Replace(";", " ");

            return userInput;
        }

        private void LoadMainForm(string username)
        {
            Form1 mainform = new Form1(username);
            mainform.Show();
        }

        private void HideCurrentForms()
        {
            this.Owner.Hide();
            this.Hide();
        }

        private void NewUserForm_Load(object sender, EventArgs e)
        {

        }

        private void newUserTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void retypePasswordTextBox_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
