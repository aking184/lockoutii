﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LockoutII
{
    public partial class UserInfoForm : Form
    {
        public string Username { get; set; }

        LockoutIIDB myLockoutIIDB = new LockoutIIDB();

        //Has username or password been changed
        private bool ChangedUser { get; set; }

        public UserInfoForm(string username)
        {
            Username = username;
            InitializeComponent();

            oldUserTextBox.Text = Username;

            ChangedUser = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            //Sanitize user input
            string newUserName = this.StripChars(newUserTextBox.Text);
            string oldPassword = this.StripChars(oldPasswordTextBox.Text);
            string newPassword = this.StripChars(newPassTextBox.Text);
            string retypePass = this.StripChars(retypeNewPassTextBox.Text);

            //Check that New Username  !=" " or null
            bool newUsername = this.NewUsername(newUserName);

            //Check that Old Password is correct
            bool correctOldPass = this.CorrectPassword();

            //Check that new password != " " or null
            bool passwordNew = this.NewPassword(newPassword);

            //Check that new password == retyped password
            bool checkPasswords = this.CheckPasswords();

            //Submit changes as necessary
            if (correctOldPass)
            { 
                //Try to update User info
                bool updatedUser = this.UpdateUsername(newUsername, newUserName);
                bool updatedPassword = this.UpdatePassword(passwordNew, checkPasswords, newPassword);
                this.ProcessUpdateOutcome(updatedUser, updatedPassword);      
            }
            else
            {
                MessageBox.Show("Unable to update user account");
            }
        }

        private void ProcessUpdateOutcome(bool updatedUser, bool updatedPassword)
        {
            if (!updatedUser && !updatedPassword)
            {
                //Nothing Got updated
                MessageBox.Show("Unable to update user account");
            }
            else
            {
                MessageBox.Show("Update successful.");
                this.ChangedUser = true;
                this.GoBackToMain();
            }
        }

        private bool UpdateUsername(bool newUser, string newUsername)
        {
            bool updatedUser = false;
            if (newUser)
            {
                //Update Username
                myLockoutIIDB.UpdateUsername(Username, newUsername);
                updatedUser = true;
                Username = newUsername;
            }
            return updatedUser;
        }

        private bool UpdatePassword(bool passwordNew, bool checkPasswords, string newPassword)
        {
            bool updatedPassword = false;

            if (passwordNew && checkPasswords)
            {
                //Update Password
                myLockoutIIDB.UpdatePassword(Username, newPassword);
                updatedPassword = true;
            }

            return updatedPassword;
        }

        private bool NewUsername(string newUsername)
        {
            bool newUser = false;

            if (newUsername != null && newUsername!= "")
            {
                newUser = true;
            }

            return newUser;
        }

        private bool CorrectPassword()
        {
            string password = this.StripChars(oldPasswordTextBox.Text);

            bool correctPassword = false;
            //Query DB from given username and its associate password
            String userPass = myLockoutIIDB.getUserPassword(Username);

            if (password == userPass)
            {
                correctPassword = true;
            }

            return correctPassword;

        }

        private string StripChars(string userInput)
        {
            //Replace apostophres to prevent SQL injections
            userInput.Replace("'", "''");

            //Check for SQL Injection attack
            userInput.Replace(";", " ");

            return userInput;
        }

        private bool NewPassword(string password)
        {
            bool NewPassword = false;
            if (password != null && password!= "")
            {
                NewPassword = true;
            }
            return NewPassword;
        }

        private bool CheckPasswords()
        {
            bool passwordMatch = false;
            if (newPassTextBox.Text == retypeNewPassTextBox.Text)
            {
                passwordMatch = true;
            }
            return passwordMatch;
        }

        private void cancelBTN_Click(object sender, EventArgs e)
        {
            this.GoBackToMain();
        }

        private void ClearForm()
        {
            newUserTextBox.Text = "";
            oldPasswordTextBox.Text = "";
            newPassTextBox.Text = "";
            retypeNewPassTextBox.Text = "";
        }

        private void GoBackToMain()
        {
            //this.ClearForm();
            this.Hide();
            this.LoadMainForm();
            this.Dispose();
        }

        private void LoadMainForm()
        {
            Form1 mainform = Application.OpenForms["Form1"] as Form1;

            //Update mainform in case changes were made
            this.UpdateMainForm(mainform);

            mainform.Enabled = true;
            mainform.Show();
        }

        private void UpdateMainForm(Form1 mainform)
        {
            if (ChangedUser)
            {
                mainform.ThisUser = Username;
                ListBuilder keyList = new ListBuilder(Username);
                mainform.KeyList = keyList;
            }
        }

        private void UserInfoForm_Load(object sender, EventArgs e)
        {

        }

        private void oldUserTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void retypeNewPassTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxPass_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPass.Checked == true)
            {
                oldPasswordTextBox.PasswordChar = (char)0;  //set passwordchar to '*'
                newPassTextBox.PasswordChar = (char)0;  //set passwordchar to '*'
                retypeNewPassTextBox.PasswordChar = (char)0;  //set passwordchar to '*'
                checkBoxPass.Text = "Hide password";
            }
            else
            {
                oldPasswordTextBox.PasswordChar = '*'; //set passwordchar to '*'
                newPassTextBox.PasswordChar = '*'; //set passwordchar to '*'
                retypeNewPassTextBox.PasswordChar = '*'; //set passwordchar to '*'
                checkBoxPass.Text = "Show password";
            }
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (this.CorrectPassword())
            {
                //Open Up Delete User Form
                if (MessageBox.Show("Are you sure you want to delete this user?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Delete User
                    myLockoutIIDB.DeleteUser(Username);
                    //Load Login Form
                    this.LoadLoginForm();
                    //Set Deleted User To True
                    this.ChangedUser = true;

                    //Close this form
                    this.Close();
                }
            }
            else
            {
                //Unable to delete user
                MessageBox.Show("Unable to delete user.");
            }
         }

        private void LoadLoginForm()
        {
            LoginForm myLogin = new LoginForm();
            myLogin.Show();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown || ChangedUser == true) return;

            // Go back to main form
            this.GoBackToMain(); 
        }

    }
}
