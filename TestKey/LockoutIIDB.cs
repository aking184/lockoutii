﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace LockoutII
{
    class LockoutIIDB
    {

        SQLiteConnection LockoutIIDBConnection = new SQLiteConnection();

        //Default Constructor for SQLiteDatabase Class.
        public LockoutIIDB()
        {
            //string dbConnection = "Data Source=lockoutii.s3db; FailIfMissing=False; Password=~LK0_t#P@S2~";
            string dbConnection = "Data Source=lockoutii.s3db; FailIfMissing=False";
            SQLiteConnection lockoutIIDBConnection = new SQLiteConnection(dbConnection);
            LockoutIIDBConnection = lockoutIIDBConnection;
        }

        //Encrypt Database
        private void SetPassword()
        {
            LockoutIIDBConnection.Open();
            //LockoutIIDBConnection.ChangePassword("~LK0_t#P@S2~");
            LockoutIIDBConnection.Close();
        }

        public void GenerateTables()
        {

            string sqlString = @"
                CREATE TABLE IF NOT EXISTS [enrollment] (
                [key_value_one] VARCHAR(20)  NOT NULL,
                [key_value_two] VARCHAR(20)  NOT NULL,
                [metric_id] VARCHAR(4)  NOT NULL,
                [mean] FLOAT  NULL,
                [standard_deviation] FLOAT  NOT NULL,
                [n] INTEGER DEFAULT '0' NOT NULL,
                [username] VARCHAR (40)  NULL,
                FOREIGN KEY (username) REFERENCES user_data(username) ON DELETE CASCADE ON UPDATE CASCADE,
                PRIMARY KEY ([key_value_one],[key_value_two],[metric_id],[username])
                );


                CREATE TABLE IF NOT EXISTS [user_data] (
                  [username] VARCHAR(40) NOT NULL DEFAULT 'username', 
                  [password] VARCHAR(40) NOT NULL, 
                  [trust_threshold] FLOAT NOT NULL DEFAULT 10, 
                  [adaptive_template] BOOLEAN NOT NULL DEFAULT 0, 
                  [threshold_distance] INTEGER NOT NULL DEFAULT 25, 
                  [IsAuthenticating] BOOLEAN NOT NULL DEFAULT 0, 
                  [IsEnrolling] BOOLEAN NOT NULL DEFAULT 1, 
                  CONSTRAINT [sqlite_autoindex_user_data_1] PRIMARY KEY ([username]));
                                  
                CREATE TABLE IF NOT EXISTS log_data(
                  [time_data] DATETIME,
                  [action] VARCHAR(4) NOT NULL,
                  [username] VARCHAR (40)  NULL,
                  FOREIGN KEY (username) REFERENCES user_data(username) ON DELETE CASCADE ON UPDATE CASCADE,
                  PRIMARY KEY ([time_data]));

                CREATE TABLE IF NOT EXISTS [template] (
                  [key_value_one] VARCHAR(20) NOT NULL, 
                  [key_value_two] VARCHAR(20) NOT NULL, 
                  [metric_id] VARCHAR(4) NOT NULL, 
                  [mean] FLOAT, 
                  [standard_deviation] FLOAT, 
                  FOREIGN KEY([key_value_one], [key_value_two], [metric_id]) REFERENCES [enrollment]([key_value_one], [key_value_two], [metric_id]), 
                  CONSTRAINT [sqlite_autoindex_template_1] PRIMARY KEY ([key_value_one], [key_value_two], [metric_id]));";

            SQLiteCommand cmd = new SQLiteCommand(sqlString, LockoutIIDBConnection);
            this.ExecuteNonQueryCommand(cmd);

            //string userDataString = "INSERT OR IGNORE INTO user_data VALUES ('username', 'password', 50, 1, 1, 0, 1);";
            //SQLiteCommand udCMD = new SQLiteCommand(userDataString, LockoutIIDBConnection);
            //this.ExecuteNonQueryCommand(udCMD);
        }


        public void buildTemplate(string username)
        {
            this.DeleteTemplate();

            string sqlstring = @"INSERT INTO template 
                                                SELECT key_value_one, key_value_two, metric_id, mean, standard_deviation
                                                FROM enrollment
                                                WHERE username = @usrname AND n>50 AND abs(standard_deviation/mean) < 0.1;";

            SQLiteCommand cmd = new SQLiteCommand(sqlstring, LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            this.ExecuteNonQueryCommand(cmd);
        }

        public void DeleteTemplate()
        {
            SQLiteCommand cmd = new SQLiteCommand("DELETE FROM template;", LockoutIIDBConnection);
            this.ExecuteNonQueryCommand(cmd);
        }

        //Delete User
        public void DeleteUser(string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("DELETE FROM user_data WHERE username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            this.ExecuteNonQueryCommand(cmd);
            this.ClearUserFromLogData(username);
        }

        //Clear usernames from log data seperately because cascade delete is buggy
        public void ClearUserFromLogData(string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("DELETE FROM log_data WHERE username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            this.ExecuteNonQueryCommand(cmd);
        }

        //Update Log table
        public void UpdateLogTable(string loginString, string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("INSERT OR IGNORE INTO log_data VALUES(datetime('now'), @lgnString, @usrName);", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@lgnString", loginString);
            cmd.Parameters.AddWithValue("@usrname", username);
            this.ExecuteNonQueryCommand(cmd);
        }

        //Update Password
        public void UpdatePassword(string username, string newPassword)
        {
            SQLiteCommand cmd = new SQLiteCommand("UPDATE user_data SET password = @newPass WHERE username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            cmd.Parameters.AddWithValue("@newPass", newPassword);
            this.ExecuteNonQueryCommand(cmd);
        }

        //Update Username
        public void UpdateUsername(string username, string newUsername)
        {
            SQLiteCommand cmd = new SQLiteCommand("UPDATE user_data SET username = @newUser WHERE username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            cmd.Parameters.AddWithValue("@newUser", newUsername);
            this.ExecuteNonQueryCommand(cmd);
        }

        //Check if Database has User
        public int HasUser(string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("SELECT COUNT(username) FROM user_data WHERE username   = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);

            int count = 0;
            LockoutIIDBConnection.Open();
            try
            {
                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    if (reader.IsDBNull(0))
                    {
                        break;
                    }
                    else
                    {
                        count = reader.GetInt16(0);
                    }

                }

                // always call Close when done reading. 
                reader.Close();
            }
            finally
            {
                // Close the connection when done with it. 
                LockoutIIDBConnection.Close();
            }

            return count;

        }

        public void AddUser(string username, string password)
        {
            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO user_data (username, password) VALUES ( @usrname, @pass);", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            cmd.Parameters.AddWithValue("@pass", password);
            this.ExecuteNonQueryCommand(cmd);
        }

        //Clear Log Table of Old Data
        public void CleanLogTable(string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("DELETE FROM log_data where username = @usrname AND time_data < datetime('now', '-1 month');", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            this.ExecuteNonQueryCommand(cmd);
        }

        //Return a String Contain all log data
        public string GetLogData(string username)
        {
            string returnString = "       Log Data " + Environment.NewLine + "      Time" + "                            Action" + Environment.NewLine;

            SQLiteCommand cmd = new SQLiteCommand("SELECT time_data, action FROM log_data WHERE username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);

            LockoutIIDBConnection.Open();
            try
            {
                SQLiteDataReader reader = cmd.ExecuteReader();

                // Always call Read before accessing data. 
                int i = 0;
                while (reader.Read())
                {

                    for (i = 0; i < reader.FieldCount; i++)
                    {


                        if (reader.IsDBNull(i))
                            break;
                        else
                        {
                            //Double.TryParse(reader[i].ToString(), out readerDouble);
                            //metrics[i] = readerDouble;
                            returnString = returnString + "      " + reader[i].ToString();
                        }

                        if (i != 0 && i % 2 != 0)
                            returnString = returnString + Environment.NewLine;

                    }
                }

                // always call Close when done reading. 
                reader.Close();
            }
            finally
            {
                // Close the connection when done with it. 
                LockoutIIDBConnection.Close();
            }

            return returnString;
        }



        //update boolean in user data
        public void UpdateUserBool(string boolName, bool updateBool, string username)
        {
            //Convert bool to int for SQL purposes
            int boolInt = updateBool ? 1 : 0;

            SQLiteCommand cmd = new SQLiteCommand("UPDATE user_data SET " + boolName + " = @blInt WHERE username =  @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            cmd.Parameters.AddWithValue("@blInt", boolInt);
            this.ExecuteNonQueryCommand(cmd);
        }

        public String getUserPassword(String username)
        {
            String userPass = "0";

            SQLiteCommand cmd = new SQLiteCommand("SELECT password FROM user_data WHERE username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);

            LockoutIIDBConnection.Open();
            try
            {
                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    if (reader.IsDBNull(0))
                    {
                        break;
                    }
                    else
                    {
                        userPass = reader.GetString(0);
                    }

                }

                // always call Close when done reading. 
                reader.Close();
            }
            finally
            {
                // Close the connection when done with it. 
                LockoutIIDBConnection.Close();
            }
            return userPass;
        }

        public void UpdateThreshold(float trustThreshold, string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("UPDATE user_data SET trust_threshold = @trstThres WHERE username =  @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            cmd.Parameters.AddWithValue("@trstThres", trustThreshold);
            this.ExecuteNonQueryCommand(cmd);
        }

       public void UpdateThresholdDistance(int thresholdDistance, string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("UPDATE user_data SET threshold_distance = @thresDist WHERE username =  @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);
            cmd.Parameters.AddWithValue("@thresDist", thresholdDistance);
            this.ExecuteNonQueryCommand(cmd);
        }

        //Given KeyValueOne, KeyValueTwo, and MetricID return an array containing mean, standard_deviation, and n
        public double[] GetTemplateData(int keyValueOne, int keyValueTwo, String metricID)
        {
            double[] metrics = new double[2];

            SQLiteCommand cmd = new SQLiteCommand("SELECT mean, standard_deviation FROM template WHERE key_value_one = " + keyValueOne + " AND key_value_two = " + keyValueTwo + " AND metric_id = @mID;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@mID", metricID);

            //cmd.Connection = LockoutIIDBConnection;
            LockoutIIDBConnection.Open();
            try
            {
                SQLiteDataReader reader = cmd.ExecuteReader();

                // Always call Read before accessing data. 
                int i = 0;
                while (reader.Read())
                {

                    for (i = 0; i < reader.FieldCount; i++)
                    {
                        if (i == metrics.Length)
                            break;
                        if (reader.IsDBNull(i))
                        {
                            metrics[i] = 0;   //-1?
                        }
                        else
                        {
                            //Double.TryParse(reader[i].ToString(), out readerDouble);
                            //metrics[i] = readerDouble;
                            metrics[i] = reader.GetDouble(i);
                        }
                    }
                }

                // always call Close when done reading. 
                reader.Close();
            }
            finally
            {
                // Close the connection when done with it. 
                LockoutIIDBConnection.Close();
            }

            return metrics;
        }


        public ArrayList GetUserData(string username)
        {
            //UserData myUser = new UserData();
            ArrayList userArrayList = new ArrayList();

            SQLiteCommand cmd = new SQLiteCommand("SELECT trust_threshold, adaptive_template, threshold_distance, isAuthenticating, IsEnrolling FROM user_data WHERE  username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@usrname", username);

            //cmd.Connection = LockoutIIDBConnection;
            LockoutIIDBConnection.Open();
            try
            {
                SQLiteDataReader reader = cmd.ExecuteReader();
                //double readerDouble = 0;  //Double to hold reader values
                // Always call Read before accessing data. 
                int i = 0;
                while (reader.Read())
                {
                    if (reader.IsDBNull(i) || reader.FieldCount < 5)
                    {
                        break;
                    }
                    else
                    {
                        //Double.TryParse(reader[0].ToString(), out readerDouble);
                        userArrayList.Add(reader.GetDouble(0));
                        userArrayList.Add(reader.GetBoolean(1));
                        userArrayList.Add(reader.GetInt16(2));
                        userArrayList.Add(reader.GetBoolean(3));
                        userArrayList.Add(reader.GetBoolean(4));
                    }

                }

                // always call Close when done reading. 
                reader.Close();
            }
            finally
            {
                // Close the connection when done with it. 
                LockoutIIDBConnection.Close();
            }
            return userArrayList;
        }

        public int ExecuteNonQueryCommand(SQLiteCommand cmd)
        {
            int rowsUpdated = 0;
            LockoutIIDBConnection.Open();

            try
            {
                rowsUpdated = cmd.ExecuteNonQuery();
                //MessageBox.Show(aff + " rows were affected.");
            }
            catch
            {
                MessageBox.Show("Error encountered during Non-Query Command operation.");
            }
            finally
            {
                LockoutIIDBConnection.Close();
            }

            //Start resetting code here//
            LockoutIIDBConnection.Close();
            return rowsUpdated;
        }

        //Given KeyValueOne, KeyValueTwo, and MetricID return an array containing mean, standard_deviation, and n
        public double[] GetMetricData(int keyValueOne, int keyValueTwo, string metricID, string username)
        {
            double[] metrics = new double[3];

            SQLiteCommand cmd = new SQLiteCommand("SELECT mean, standard_deviation, n FROM enrollment WHERE key_value_one = " + keyValueOne + " AND key_value_two = " + keyValueTwo + " AND metric_id = @mID AND username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@mID", metricID);
            cmd.Parameters.AddWithValue("@usrname", username);

            //cmd.Connection = LockoutIIDBConnection;
            LockoutIIDBConnection.Open();
            try
            {
                SQLiteDataReader reader = cmd.ExecuteReader();

                // Always call Read before accessing data. 
                int i = 0;
                while (reader.Read())
                {

                    for (i = 0; i < reader.FieldCount; i++)
                    {
                        if (i == metrics.Length)
                            break;
                        if (reader.IsDBNull(i))
                        {
                            metrics[i] = 0;   //-1?
                        }
                        else
                        {
                            //Double.TryParse(reader[i].ToString(), out readerDouble);
                            //metrics[i] = readerDouble;
                            metrics[i] = reader.GetDouble(i);
                        }
                    }
                }

                // always call Close when done reading. 
                reader.Close();
            }
            finally
            {
                // Close the connection when done with it. 
                LockoutIIDBConnection.Close();
            }

            return metrics;
        }

        //Update Enrollment Table KeyMetric (mean, standard_deviation, and n)
        public void updateKeyMetric(double keyValueOne, double keyValueTwo, String metricID, double mean, double standard_deviation, int n, string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("Update enrollment SET mean = @mean, standard_deviation = @stanDev, n = @n WHERE key_value_one = " + keyValueOne + " AND key_value_two = " + keyValueTwo + " AND metric_id = @mID AND username = @usrname;", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@mean", mean);
            cmd.Parameters.AddWithValue("@stanDev", standard_deviation);
            cmd.Parameters.AddWithValue("@n", n);
            cmd.Parameters.AddWithValue("@mID", metricID);
            cmd.Parameters.AddWithValue("@usrname", username);

            int rowsUpdated = this.ExecuteNonQueryCommand(cmd);

            if (rowsUpdated <= 0)
            {
                //Add data two table in a new row
                this.AddKeyMetric(keyValueOne, keyValueTwo, metricID, mean, standard_deviation, n, username);
            }
        }

        public void AddKeyMetric(double keyValueOne, double keyValueTwo, String metricID, double mean, double standard_deviation, int n, string username)
        {
            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO enrollment Values ( " + keyValueOne + ", " + keyValueTwo + ", @mID, @mean, @stanDev, @n, @usrname);", LockoutIIDBConnection);
            cmd.Parameters.AddWithValue("@mean", mean);
            cmd.Parameters.AddWithValue("@stanDev", standard_deviation);
            cmd.Parameters.AddWithValue("@n", n);
            cmd.Parameters.AddWithValue("@mID", metricID);
            cmd.Parameters.AddWithValue("@usrname", username);

            int rowsUpdated = this.ExecuteNonQueryCommand(cmd);
        }


    }
}