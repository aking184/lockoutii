﻿namespace LockoutII
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.isAuthenticatingCheckBox = new System.Windows.Forms.CheckBox();
            this.adaptiveLearning = new System.Windows.Forms.CheckBox();
            this.thresholdTrackBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.trustThreshold = new System.Windows.Forms.Label();
            this.btnLogData = new System.Windows.Forms.Button();
            this.isEnrollingCheckBox = new System.Windows.Forms.CheckBox();
            this.userInfoBtn = new System.Windows.Forms.Button();
            this.thresholdDistance = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdDistance)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(184, 282);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.MaximumSize = new System.Drawing.Size(80, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Confidence";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 282);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Confidence Level:";
            // 
            // textBox1
            // 
            this.textBox1.AcceptsReturn = true;
            this.textBox1.AcceptsTab = true;
            this.textBox1.Location = new System.Drawing.Point(26, 24);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(236, 122);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // isAuthenticatingCheckBox
            // 
            this.isAuthenticatingCheckBox.AutoSize = true;
            this.isAuthenticatingCheckBox.Location = new System.Drawing.Point(26, 218);
            this.isAuthenticatingCheckBox.Name = "isAuthenticatingCheckBox";
            this.isAuthenticatingCheckBox.Size = new System.Drawing.Size(109, 21);
            this.isAuthenticatingCheckBox.TabIndex = 3;
            this.isAuthenticatingCheckBox.Text = "Authenticate";
            this.isAuthenticatingCheckBox.UseVisualStyleBackColor = true;
            this.isAuthenticatingCheckBox.CheckedChanged += new System.EventHandler(this.isAuthenticatingCheckBox_CheckedChanged);
            // 
            // adaptiveLearning
            // 
            this.adaptiveLearning.AutoSize = true;
            this.adaptiveLearning.Location = new System.Drawing.Point(26, 245);
            this.adaptiveLearning.Name = "adaptiveLearning";
            this.adaptiveLearning.Size = new System.Drawing.Size(137, 21);
            this.adaptiveLearning.TabIndex = 4;
            this.adaptiveLearning.Text = "Adaptive Learing";
            this.adaptiveLearning.UseVisualStyleBackColor = true;
            this.adaptiveLearning.CheckedChanged += new System.EventHandler(this.adaptiveLearning_CheckedChanged);
            // 
            // thresholdTrackBar
            // 
            this.thresholdTrackBar.Location = new System.Drawing.Point(12, 344);
            this.thresholdTrackBar.Maximum = 100;
            this.thresholdTrackBar.Name = "thresholdTrackBar";
            this.thresholdTrackBar.Size = new System.Drawing.Size(250, 56);
            this.thresholdTrackBar.TabIndex = 5;
            this.thresholdTrackBar.TickFrequency = 10;
            this.thresholdTrackBar.Scroll += new System.EventHandler(this.thresholdTrackBar_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 315);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Trust Threshold:";
            // 
            // trustThreshold
            // 
            this.trustThreshold.AutoSize = true;
            this.trustThreshold.Location = new System.Drawing.Point(184, 315);
            this.trustThreshold.Name = "trustThreshold";
            this.trustThreshold.Size = new System.Drawing.Size(72, 17);
            this.trustThreshold.TabIndex = 7;
            this.trustThreshold.Text = "Threshold";
            // 
            // btnLogData
            // 
            this.btnLogData.Location = new System.Drawing.Point(187, 191);
            this.btnLogData.Name = "btnLogData";
            this.btnLogData.Size = new System.Drawing.Size(75, 29);
            this.btnLogData.TabIndex = 10;
            this.btnLogData.Text = "Log Data";
            this.btnLogData.UseVisualStyleBackColor = true;
            this.btnLogData.Click += new System.EventHandler(this.btnLogData_Click);
            // 
            // isEnrollingCheckBox
            // 
            this.isEnrollingCheckBox.AutoSize = true;
            this.isEnrollingCheckBox.Location = new System.Drawing.Point(26, 191);
            this.isEnrollingCheckBox.Name = "isEnrollingCheckBox";
            this.isEnrollingCheckBox.Size = new System.Drawing.Size(66, 21);
            this.isEnrollingCheckBox.TabIndex = 11;
            this.isEnrollingCheckBox.Text = "Enroll";
            this.isEnrollingCheckBox.UseVisualStyleBackColor = true;
            this.isEnrollingCheckBox.CheckedChanged += new System.EventHandler(this.isEnrollingCheckBox_CheckedChanged);
            // 
            // userInfoBtn
            // 
            this.userInfoBtn.Location = new System.Drawing.Point(187, 237);
            this.userInfoBtn.Name = "userInfoBtn";
            this.userInfoBtn.Size = new System.Drawing.Size(75, 29);
            this.userInfoBtn.TabIndex = 12;
            this.userInfoBtn.Text = "User Info";
            this.userInfoBtn.UseVisualStyleBackColor = true;
            this.userInfoBtn.Click += new System.EventHandler(this.userInfoBtn_Click);
            // 
            // thresholdDistance
            // 
            this.thresholdDistance.Location = new System.Drawing.Point(187, 158);
            this.thresholdDistance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.thresholdDistance.Name = "thresholdDistance";
            this.thresholdDistance.Size = new System.Drawing.Size(76, 22);
            this.thresholdDistance.TabIndex = 13;
            this.thresholdDistance.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.thresholdDistance.ValueChanged += new System.EventHandler(this.thresholdDistance_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Threshold Distance:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 393);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.thresholdDistance);
            this.Controls.Add(this.userInfoBtn);
            this.Controls.Add(this.isEnrollingCheckBox);
            this.Controls.Add(this.btnLogData);
            this.Controls.Add(this.trustThreshold);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.thresholdTrackBar);
            this.Controls.Add(this.adaptiveLearning);
            this.Controls.Add(this.isAuthenticatingCheckBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "LockoutII";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.thresholdTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thresholdDistance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox isAuthenticatingCheckBox;
        private System.Windows.Forms.CheckBox adaptiveLearning;
        private System.Windows.Forms.TrackBar thresholdTrackBar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label trustThreshold;
        private System.Windows.Forms.Button btnLogData;
        private System.Windows.Forms.CheckBox isEnrollingCheckBox;
        private System.Windows.Forms.Button userInfoBtn;
        private System.Windows.Forms.NumericUpDown thresholdDistance;
        private System.Windows.Forms.Label label4;


    }
}

