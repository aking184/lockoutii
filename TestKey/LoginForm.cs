﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LockoutII
{
    public partial class LoginForm : Form
    {

       LockoutIIDB myLockoutIIDB = new LockoutIIDB();
        
        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPass_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBoxPass_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBoxPass.Checked == true)
            {
                txtPass.PasswordChar = (char)0; //reset passwordchar to default
                checkBoxPass.Text = "Hide password"; 
            }
            else
            {
                txtPass.PasswordChar = '*'; //set passwordchar to '*'
                checkBoxPass.Text = "Show password"; 
            }

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            String inputUser = this.StripChars(txtUser.Text);

            bool isPass = this.passwordCheck(inputUser);

            //if (txtUser.Text.ToString().CompareTo("test") == 0 & txtPass.Text.ToString().CompareTo("test") == 0)
            if (isPass == true)
            {
                //MessageBox.Show("Login Sucessful. \n Click OK to continue", "Login", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.HideLogin();     //hide the login form
                this.LoadMainForm(inputUser);
            }
            else
            {
                MessageBox.Show("Incorrect username or password.", "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPass.Text = ""; //delete password
                txtUser.Text = ""; //delete username
            }
        }


        private void HideLogin()
        {
            txtPass.Text = ""; //delete password
            txtUser.Text = ""; //delete username
            this.Hide();   
        }


        private bool passwordCheck(string inputUser)
        {
            //Query DB from given username and its associate password
            String userPass = myLockoutIIDB.getUserPassword(inputUser);

           //Check if userPass was found
           if (userPass == "0")
           {
               return false;
           }
           //Check if DB password matches userPassword
           else if (txtPass.Text.ToString().CompareTo(userPass) == 0)
           {
               return true;
           }
           //DB password does not match user password
           else
           {
               return false;
           }
            
        }

        private void LoadMainForm(string inputUser)
        {
            Form1 mainform = new Form1(inputUser);
            mainform.Show();
        }

        private string StripChars(string userInput)
        {
            //Replace apostophres to prevent SQL injections
            userInput.Replace("'", "''");

            //Check for SQL Injection attack
            userInput.Replace(";", " ");

            return userInput;
        }

        private void LoadNewUserForm()
        {
            NewUserForm myNewUserForm = new NewUserForm();
            myNewUserForm.ShowDialog(this);
        }

        private void btnNewUser_Click(object sender, EventArgs e)
        {
            //this.HideLogin();
            this.LoadNewUserForm();
        }

    }
}


