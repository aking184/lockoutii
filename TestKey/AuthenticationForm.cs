﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LockoutII
{
    public partial class AuthenticationForm : Form
    {
        public string Username { get; set; }
        LockoutIIDB myLockoutIIDB = new LockoutIIDB();

        public AuthenticationForm(string username)
        {
            Username = username;
            InitializeComponent();
        }

        private void AuthenticationForm_Load(object sender, EventArgs e)
        {

        }

        private void checkBoxPass_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxPass.Checked == true)
            {
                passwordTextBox.PasswordChar = (char)0;  //set passwordchar to '*'
                checkBoxPass.Text = "Hide password";
            }
            else
            {
                passwordTextBox.PasswordChar = '*'; //set passwordchar to '*'
                checkBoxPass.Text = "Show password";
            }
        }

        private void passwordTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private bool passwordCheck()
        {
            //Query DB from given username and its associate password
            String userPass = myLockoutIIDB.getUserPassword(Username);

            //Check if userPass was found
            if (userPass == "0")
            {
                return false;
            }
            //Check if DB password matches userPassword
            else if (passwordTextBox.Text.ToString().CompareTo(userPass) == 0)
            {
                return true;
            }
            //DB password does not match user password
            else
            {
                return false;
            }

        }

        private void Submit_Click(object sender, EventArgs e)
        {

            bool isAuthenticated = this.passwordCheck();

            if (isAuthenticated == true)
            {
                //MessageBox.Show("Login Sucessful. \n Click OK to continue", "Login", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                this.LoadMainForm();
                this.Dispose();
            }
            else
            {
                MessageBox.Show("Incorrect password.", "Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                passwordTextBox.Text = ""; //delete password
            }
        }

        private void GoBackToMain()
        {
            //When user clicks exit, keep all forms hidden
            this.Hide();
            Form1 mainform = Application.OpenForms["Form1"] as Form1;
            this.Dispose();
        }

        private void LoadMainForm()
        {
            Form1 mainform = Application.OpenForms["Form1"] as Form1;
            mainform.Show();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Go back to main form
            this.GoBackToMain();
        }


    }
}
