# LockoutII
## About:
		LockoutII is a keystroke biometric authentication software.  
		This provides an added layer of security, by authenticating users based on their unique typing patterns.   If a user is determined to be an imposter, the system will lockout that user.

## Source Code:
		The source code can be found at the following website:
		https://bitbucket.org/aking184/lockoutii
		
## OS Requirements:
		Windows

## Install Steps:
	1.  Download LockoutII from 
		http://users.marshall.edu/~king184/LockoutII.html
	2.  Run setup.exe and click install.
	3.  LockoutII will immeadiately run. 

## First Time Users:
	1.  Run LockoutII in enroll mode 
	2.  Go about your usual business.
	3.  Use F11 to show the LockoutII dialog and F8 to hide the LockoutII dialog.
	4.  After a couple days of enrollment, you can begin authenticating.
	5.  Check the authenticating checkbox, to use LockoutII for Authentication.
	6.  Set the Lockout Distance and trust threshold to an appropriate distance, so that you are not locked out but others will be.
	6.  To properly use LockoutII, the user should set LockoutII to launch on startup.

##Set Launch on Startup:
	1.  Click the Start button, click All Programs, right-click the Startup folder, and then click Open.
    2.  Open the location that contains the item to which you want to create a shortcut.
    3.  Right-click the item, and then click Create Shortcut. 
	4.  The new shortcut appears in the same location as the original item.
    5.  Drag the shortcut into the Startup folder.
	6.  Alternatively, use msconfig.exe to configure what launches on startup.
	
##User Controls
	1.  The trust threshold is the minimum amount of trust LockoutII has that you are the genuine user.
		After that point, LockoutII will lock the system.
	2.  Threshold distance the maximum allowable distance (in ms) from the stored user template.  
		Any distance less than the threshold distance will increase the confidence level, and anything greater than or equal to the threshold distance will decrease the confidence level.
	
##Troubleshooting:
	I.  I keep getting locked out!
		1. Try running the enrollment phase a little longer. 
		   This will help LockoutII get a more accurate picture of your typing.
		2. Try lowering the trust threshold.  
			The lower the trust threshold, the less likely genuine users will be locked out and the less likely imposters will get caught.  The higher the trust threshold, the more likely genuine users will be locked out and the more likely imposters will get caught.
	
	II. LockoutII is not showing!
		1.  LockoutII is hidden be default when authenticating.
		2.  Use F11 to show the LockoutII dialog and F8 to hide the LockoutII dialog.
	
	III. LockoutII does not automaticatlly start when I log on.
		1.  For this version of LockoutII you may have to mannually add LockoutII to your Startup folder or use msconfig.exe
		2.  See "Set Launch on Startup" above.
	
	IV.  The Lockout Time Does not work!
		1.  Future versions of the software will allow users to specify a specific length of time.
		2.  Although the user interface for setting lockout times is present, it is not yet functional.
		
	

## Frequently Asked Questions:
	I. What typing would be best for enrollment?
		The best way you can enroll in LockoutII is to type how you normally do.
		The most frequently typed texts (such as login information) work best for authentication.



	


